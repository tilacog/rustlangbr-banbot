use arrayvec::ArrayVec;
use chrono::{NaiveDateTime, Utc};
use std::collections::HashMap;

type TelegramUserId = u32; // TODO: check it Telegram stores user ids as a u32
type Message = String; // TODO: which type is it?

#[derive(Debug, PartialEq)]
enum Action {
    Ban,
    Kick,
    Greet,
    Trust,
}

#[derive(Hash, Eq, PartialEq, Debug)]
struct UserData {
    date_joined: NaiveDateTime,
    greeted: bool,
    messages: ArrayVec<[String; 20]>,
}

impl UserData {
    fn new() -> Self {
        Self {
            date_joined: Utc::now().naive_utc(),
            greeted: false,
            messages: ArrayVec::new(),
        }
    }

    fn handle(&self) -> Option<(Action)> {
        unimplemented!()
    }
}

#[derive(Debug)]
pub struct Quarantine(HashMap<TelegramUserId, UserData>);

impl Quarantine {
    pub fn new() -> Self {
        Self(HashMap::new())
    }
    pub fn add_user(&mut self, user_id: TelegramUserId) {
        let user_data = UserData::new();
        self.0.insert(user_id, user_data);
    }

    pub fn routine_check(&mut self) {
        let mut trusted_users: Vec<TelegramUserId> = vec![];

        for (user_id, user_data) in &self.0 {
            if let Some(action) = user_data.handle() {
                use Action::*;
                match action {
                    Ban => unimplemented!(),
                    Kick => unimplemented!(),
                    Greet => unimplemented!(),
                    Trust => trusted_users.push(*user_id),
                };
            }
        }

        for user in trusted_users.iter() {
            self.0.remove(user);
        }
    }
}

#[cfg(test)]
mod tests {
    // TODO Use real Telegram types instead of Strings
    use super::*;
    use chrono::Duration;

    #[test]
    fn kick_silent_user_after_a_day() {
        let mut user = UserData::new();
        user.date_joined = user.date_joined - Duration::days(1); // rewind one day
        assert_eq!(user.handle(), Some(Action::Kick))
    }

    #[test]
    fn trust_talkative_user_after_a_day() {
        let mut user = UserData::new();
        user.messages.push("hello, world!".to_string()); // make user say something
        user.date_joined = user.date_joined + Duration::days(1); // rewind one day
        assert_eq!(user.handle(), Some(Action::Trust))
    }

    #[test]
    fn ban_user_when_its_first_message_has_a_link() {
        let mut user = UserData::new();
        let spam = "http://facebook.com".to_string();
        user.messages.push(spam);
        assert_eq!(user.handle(), Some(Action::Ban))
    }
}
